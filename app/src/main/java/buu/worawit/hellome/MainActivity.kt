package buu.worawit.hellome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val edtName = findViewById<EditText>(R.id.edtName)
        val btnHello = findViewById<Button>(R.id.btnHello)
//        btnHello.setOnClickListener(this)
        btnHello.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View?) {
                Toast.makeText(this@MainActivity, "Click Me", Toast.LENGTH_LONG).show()
            }

        })
    }

    override fun onClick(view: View?) {
        Toast.makeText(this, "Click Me", Toast.LENGTH_LONG).show()
    }
}